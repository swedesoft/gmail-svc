package com.digitalreasoning.gmailsvc

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.integration.config.EnableIntegration
import org.springframework.integration.dsl.IntegrationFlow
import org.springframework.integration.dsl.IntegrationFlows
import org.springframework.integration.mail.SearchTermStrategy
import org.springframework.integration.mail.dsl.Mail
import org.springframework.integration.dsl.MessageChannels
import org.springframework.messaging.PollableChannel
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.integration.mail.support.DefaultMailHeaderMapper
import org.springframework.integration.mapping.HeaderMapper
import org.springframework.mail.MailMessage
import org.springframework.mail.javamail.MimeMailMessage
import javax.mail.Flags
import javax.mail.internet.MimeMessage
import javax.mail.Flags.Flag.SEEN
import javax.mail.search.FlagTerm
import javax.mail.search.AndTerm
import javax.mail.internet.InternetAddress
import javax.mail.search.FromTerm
import javax.mail.Folder
import javax.mail.internet.AddressException
import javax.mail.search.SearchTerm




@EnableIntegration
@SpringBootApplication
class DemoApplication {

	@Autowired
	private val imapIdleChannel: PollableChannel? = null

	@Bean
	fun mailHeaderMapper(): HeaderMapper<MimeMessage> {
		return DefaultMailHeaderMapper()
	}

	@Bean
	fun imapIdleFlow(): IntegrationFlow {
		return IntegrationFlows
				.from(Mail.imapIdleAdapter("imap://user:pw@localhost:" + "80" + "/INBOX")
						.autoStartup(true)
						.searchTermStrategy { this.fromAndNotSeenTerm() }
						.userFlag("testSIUserFlag")
						.javaMailProperties { p ->
							p.put("mail.debug", "false")
									.put("mail.imap.connectionpoolsize", "5")
						}
						.shouldReconnectAutomatically(false)
						.headerMapper(mailHeaderMapper()))
				.channel(MessageChannels.queue("imapIdleChannel"))
				.get()
	}

	private fun fromAndNotSeenTerm(supportedFlags: Flags, folder: Folder): SearchTerm {
		try {
			val fromTerm = FromTerm(InternetAddress("bar@baz"))
			return AndTerm(fromTerm, FlagTerm(Flags(Flags.Flag.SEEN), false))
		} catch (e: AddressException) {
			throw RuntimeException(e)
		}

	}

}

fun main(args: Array<String>) {
	runApplication<DemoApplication>(*args)
}


