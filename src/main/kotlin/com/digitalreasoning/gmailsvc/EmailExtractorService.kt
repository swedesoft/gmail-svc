package com.digitalreasoning.gmailsvc

import mu.KotlinLogging
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct


@Service
class EmailExtractorService {

    private val log = KotlinLogging.logger {}

    @PostConstruct
    fun start() {
        log.debug { "Starting up the email extractor service." }
    }
}